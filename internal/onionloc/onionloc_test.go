package onionloc

import (
	"io"
	"net/http"
	"strings"
	"testing"
)

func TestHTML(t *testing.T) {
	for _, table := range []struct {
		desc string
		html string
		want string
	}{
		{"onion location set", htmlNoOnionLocation, ""},
		{"onion location unset", htmlOnionLocation, htmlOnionLocationValue},
	} {
		var r http.Response
		r.Body = io.NopCloser(strings.NewReader(table.html))

		v, ok := HTML(&r)
		if got, want := ok, table.want != ""; got != want {
			t.Errorf("%s: got ok %v but wanted %v", table.desc, got, want)
			continue
		}
		if !ok {
			continue
		}
		if got, want := v, table.want; got != want {
			t.Errorf("%s: got %s but wanted %s", table.desc, got, want)
		}
	}
}

// curl -s https://www.rgdd.se/
const htmlNoOnionLocation = `<!DOCTYPE html>
<html lang=""><head>
	<meta name="generator" content="Hugo 0.105.0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>rgdd.se</title>
    <meta name="description" content="A simple monospaced resume theme for Hugo.">
    <meta name="author" content='Rasmus Dahlberg'>

    <link rel="stylesheet" href="/cdnjs/bootstrap.min.css">
    <link rel="stylesheet" href="/cdnjs/all.min.css">
    <link rel="stylesheet" href="/fontawesome/all.css">

    
    <link rel="stylesheet" href="/sass/researcher.min.css">

    

    <link rel="alternate" type="application/rss+xml" href="https://www.rgdd.se/index.xml" title="rgdd.se" />
    
        
    
</head>

    <body><div class="container mt-5">
    <nav class="navbar navbar-expand-sm flex-column flex-sm-row text-nowrap p-0">
        <a class="navbar-brand mx-0 mr-sm-auto" href="https://www.rgdd.se" title="rgdd.se">
          
          rgdd.se
        </a>
        <div class="navbar-nav flex-row flex-wrap justify-content-center">
            
                
                
                    <a class="nav-item nav-link" href="/post" title="Blog">
                        Blog
                    </a>
                    
                
            
        </div>
    </nav>
</div>
<hr>
<div id="content">
<div class="container">
    <figure class="avatar"><img src="img/author.jpg"
         alt="avatar"/>
</figure>

<p>PhD student at <a href="https://www.kau.se/en/">Karlstad University</a>.  Software engineer at <a href="https://www.glasklarteknik.se/">Glasklar Teknik</a>.
I am passionate about transparency logs, anonymity networks, and Linux.  I have
a keen interest for the overlap between research, engineering, and operations.
Off-work I cook, walk, socialize, lift weights, and spoil my cat.</p>
<h1 id="project-involvement">Project involvement</h1>
<p>I am a core member of the following projects:</p>
<ul>
<li><a href="https://www.sigsum.org/">Sigsum</a>: a free and open source software project that makes a signer&rsquo;s
key-usage transparent.  It can be used as a building block to secure the
supply chain and more.</li>
<li><a href="https://www.system-transparency.org/">System Transparency</a>: an open source project that provides a security
architecture for bare metal servers.  A system&rsquo;s entire boot chain becomes
protected and auditable.</li>
<li><a href="https://www.torproject.org/">Tor</a>: a 501(c)(3) US nonprofit that advance human rights and defend online
privacy through free software and open networks.</li>
</ul>
<h1 id="selected-publications">Selected publications</h1>
<ul>
<li>Rasmus Dahlberg, Tobias Pulls.
<a href="https://www.usenix.org/conference/usenixsecurity23/presentation/dahlberg">Timeless Timing Attacks and Preload Defenses in Tor&rsquo;s DNS Cache</a>.
USENIX Security (2023)</li>
<li>Rasmus Dahlberg, Tobias Pulls, Tom Ritter, and Paul Syverson.
<a href="https://petsymposium.org/2021/files/papers/issue2/popets-2021-0024.pdf">Privacy-Preserving &amp; Incrementally-Deployable Support for Certificate Transparency in Tor</a>.
PETS (2021)</li>
<li>Tobias Pulls and Rasmus Dahlberg.
<a href="https://petsymposium.org/2020/files/papers/issue1/popets-2020-0013.pdf">Website Fingerprinting with Website Oracles</a>.
PETS (2020)</li>
<li>Rasmus Dahlberg, Tobias Pulls, Jonathan Vestin, Toke Høiland-Jørgensen, and Andreas Kassler.
<a href="https://arxiv.org/pdf/1806.08817.pdf">Aggregation-Based Certificate Transparency Gossip</a>.
SECUREWARE (2019)</li>
<li>Tobias Pulls and Rasmus Dahlberg.
<a href="https://eprint.iacr.org/2018/737.pdf">Steady: A Simple End-to-End Secure Logging System</a>.
NordSec (2018)</li>
</ul>
<h1 id="contact">Contact</h1>
<p>Feel free to reach out on email or other platforms as you see fit.</p>

</div>

        </div><div id="footer" class="mb-5">
    <hr>
    
    <div class="container text-center">
        
            <a href="https://www.instagram.com/__rgdd" class="fab fa-instagram fa-1x" title="Instagram"></a>
        
            <a href="https://mastodon.social/@rgdd" class="fab fa-mastodon fa-1x" title="Mastodon"></a>
        
            <a href="https://github.com/rgdd" class="fab fa-github fa-1x" title="GitHub"></a>
        
            <a href="https://gitlab.torproject.org/rgdd" class="fab fa-gitlab fa-1x" title="GitLab"></a>
        
            <a href="https://git.glasklar.is/rgdd" class="fab fa-gitlab fa-1x" title="GitLab"></a>
        
            <a href="mailto:rasmus@rgdd.se" class="fas fa-envelope fa-1x" title="E-mail"></a>
        
    </div>
    
    
        <div class="container text-center">
            <a href="http://cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion" title="cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion"><small>cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion</small></a>
        </div>
    
</div>
</body>
</html>`

const htmlOnionLocationValue = "http://cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion/"

// curl -s https://www.rgdd.se/
// then add <meta http-equiv="onion-location" content="http://cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion/" />
// somewhere in the head portion
const htmlOnionLocation = `<!DOCTYPE html>
<html lang=""><head>
	<meta name="generator" content="Hugo 0.105.0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>rgdd.se</title>
    <meta name="description" content="A simple monospaced resume theme for Hugo.">
    <meta name="author" content='Rasmus Dahlberg'>
    <meta http-equiv="onion-location" content="http://cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion/" />

    <link rel="stylesheet" href="/cdnjs/bootstrap.min.css">
    <link rel="stylesheet" href="/cdnjs/all.min.css">
    <link rel="stylesheet" href="/fontawesome/all.css">

    
    <link rel="stylesheet" href="/sass/researcher.min.css">

    

    <link rel="alternate" type="application/rss+xml" href="https://www.rgdd.se/index.xml" title="rgdd.se" />
    
        
    
</head>

    <body><div class="container mt-5">
    <nav class="navbar navbar-expand-sm flex-column flex-sm-row text-nowrap p-0">
        <a class="navbar-brand mx-0 mr-sm-auto" href="https://www.rgdd.se" title="rgdd.se">
          
          rgdd.se
        </a>
        <div class="navbar-nav flex-row flex-wrap justify-content-center">
            
                
                
                    <a class="nav-item nav-link" href="/post" title="Blog">
                        Blog
                    </a>
                    
                
            
        </div>
    </nav>
</div>
<hr>
<div id="content">
<div class="container">
    <figure class="avatar"><img src="img/author.jpg"
         alt="avatar"/>
</figure>

<p>PhD student at <a href="https://www.kau.se/en/">Karlstad University</a>.  Software engineer at <a href="https://www.glasklarteknik.se/">Glasklar Teknik</a>.
I am passionate about transparency logs, anonymity networks, and Linux.  I have
a keen interest for the overlap between research, engineering, and operations.
Off-work I cook, walk, socialize, lift weights, and spoil my cat.</p>
<h1 id="project-involvement">Project involvement</h1>
<p>I am a core member of the following projects:</p>
<ul>
<li><a href="https://www.sigsum.org/">Sigsum</a>: a free and open source software project that makes a signer&rsquo;s
key-usage transparent.  It can be used as a building block to secure the
supply chain and more.</li>
<li><a href="https://www.system-transparency.org/">System Transparency</a>: an open source project that provides a security
architecture for bare metal servers.  A system&rsquo;s entire boot chain becomes
protected and auditable.</li>
<li><a href="https://www.torproject.org/">Tor</a>: a 501(c)(3) US nonprofit that advance human rights and defend online
privacy through free software and open networks.</li>
</ul>
<h1 id="selected-publications">Selected publications</h1>
<ul>
<li>Rasmus Dahlberg, Tobias Pulls.
<a href="https://www.usenix.org/conference/usenixsecurity23/presentation/dahlberg">Timeless Timing Attacks and Preload Defenses in Tor&rsquo;s DNS Cache</a>.
USENIX Security (2023)</li>
<li>Rasmus Dahlberg, Tobias Pulls, Tom Ritter, and Paul Syverson.
<a href="https://petsymposium.org/2021/files/papers/issue2/popets-2021-0024.pdf">Privacy-Preserving &amp; Incrementally-Deployable Support for Certificate Transparency in Tor</a>.
PETS (2021)</li>
<li>Tobias Pulls and Rasmus Dahlberg.
<a href="https://petsymposium.org/2020/files/papers/issue1/popets-2020-0013.pdf">Website Fingerprinting with Website Oracles</a>.
PETS (2020)</li>
<li>Rasmus Dahlberg, Tobias Pulls, Jonathan Vestin, Toke Høiland-Jørgensen, and Andreas Kassler.
<a href="https://arxiv.org/pdf/1806.08817.pdf">Aggregation-Based Certificate Transparency Gossip</a>.
SECUREWARE (2019)</li>
<li>Tobias Pulls and Rasmus Dahlberg.
<a href="https://eprint.iacr.org/2018/737.pdf">Steady: A Simple End-to-End Secure Logging System</a>.
NordSec (2018)</li>
</ul>
<h1 id="contact">Contact</h1>
<p>Feel free to reach out on email or other platforms as you see fit.</p>

</div>

        </div><div id="footer" class="mb-5">
    <hr>
    
    <div class="container text-center">
        
            <a href="https://www.instagram.com/__rgdd" class="fab fa-instagram fa-1x" title="Instagram"></a>
        
            <a href="https://mastodon.social/@rgdd" class="fab fa-mastodon fa-1x" title="Mastodon"></a>
        
            <a href="https://github.com/rgdd" class="fab fa-github fa-1x" title="GitHub"></a>
        
            <a href="https://gitlab.torproject.org/rgdd" class="fab fa-gitlab fa-1x" title="GitLab"></a>
        
            <a href="https://git.glasklar.is/rgdd" class="fab fa-gitlab fa-1x" title="GitLab"></a>
        
            <a href="mailto:rasmus@rgdd.se" class="fas fa-envelope fa-1x" title="E-mail"></a>
        
    </div>
    
    
        <div class="container text-center">
            <a href="http://cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion" title="cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion"><small>cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion</small></a>
        </div>
    
</div>
</body>
</html>`
