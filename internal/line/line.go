package line

import "sync"

type Line struct {
	sync.Mutex
	num int64
}

func (l *Line) Inc() {
	l.Lock()
	defer l.Unlock()

	l.num++
}

func (l *Line) Num() int64 {
	l.Lock()
	defer l.Unlock()

	return l.num
}
