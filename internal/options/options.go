package options

import (
	"flag"
	"time"
)

type Options struct {
	InputFile       string
	NumWorkers      int
	Limit           int
	MaxResponse     int64
	Timeout         time.Duration
	MetricsInterval time.Duration
}

func Parse() (opts Options) {
	flag.StringVar(&opts.InputFile, "i", "", "input file, one domain name per line")
	flag.IntVar(&opts.NumWorkers, "w", 128, "number of parallel workers")
	flag.IntVar(&opts.Limit, "l", 64, "rate-limit that kicks in before handing out work in requests/s")
	flag.Int64Var(&opts.MaxResponse, "r", 16, "max response body size to accept in MiB")
	flag.DurationVar(&opts.Timeout, "t", 5*time.Second, "timeout for each website visit")
	flag.DurationVar(&opts.MetricsInterval, "m", 5*time.Second, "how often to emit metrics")
	flag.Parse()
	return
}
