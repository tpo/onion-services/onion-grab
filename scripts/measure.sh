#!/bin/bash

#
# A script that performs a single onion-grab measurement with Mullvad VPN.
#
# Don't forget to shuffle the input file before running, and to document how the
# system is configured.  E.g., systemd-resolved is disabled, /etc/resolv.conf
# lists 8.8.8.8 and 8.8.4.4, output of uname -a and sysctl -a is ..., etc.
#

relay_country=se
relay_city=sto
limit=1450
num_workers=10000
input_file=example.lst
timeout_s=30
response_max_mib=64
metrics_interval=1h

out_dir=data/$(date +%Y%m%d-%H%M%S)
mullvad_wait_s=5

set -eu
function main() {
	num_sites=$(wc -l $input_file | cut -d' ' -f1)
	debug "relay_country=$relay_country"
	debug "relay_city=$relay_city"
	debug "limit=${limit}"
	debug "num_workers=$num_workers"
	debug "input_file=$input_file ($num_sites sites)"
	debug "timeout_s=$timeout_s"
	debug "response_max_mib=$response_max_mib"
	debug "out_dir=$out_dir"
	debug "mullvad_wait_s=$mullvad_wait_s"

	mkdir -p "$out_dir"
	stdout_file=$out_dir/$relay_country-$relay_city-l$limit.stdout
	stderr_file=$out_dir/$relay_country-$relay_city-l$limit.stderr
	info "storing results in $out_dir"

	now=$(date +%s)
	runtime_s=$(( $num_sites / $limit ))
	wait_onion_grab_s=$(( $timeout_s * 2 ))
	estimated_done=$(( $now + $runtime_s + $mullvad_wait_s + $wait_onion_grab_s ))
	debug "estimated done? approximately $(date -d @$estimated_done +"%Y-%m-%d %H:%M:%S %Z")"

	mullvad disconnect >/dev/null
	mullvad relay set location $relay_country $relay_city >/dev/null
	mullvad connect >/dev/null
	sleep $mullvad_wait_s

	ip=$(curl -s https://ifconfig.me)
	info "starting measurement from relay with ip $ip"
	debug "view progress with \"tail -f $stderr_file\""

	onion-grab\
	     	-i "$input_file"\
		-w "$num_workers"\
		-l "$limit"\
		-r "$response_max_mib"\
		-t "$timeout_s"s\
		-m "$metrics_interval"\
		>"$stdout_file" 2>"$stderr_file"

	./digest.py\
		-v info\
		-i "$stdout_file"\
		-o "$out_dir/$relay_country-$relay_city-l$limit-onion-all.txt"\
		-d "$out_dir/$relay_country-$relay_city-l$limit-domain-all.txt"
}

function debug() { echo "$(date +"%Y-%m-%d %H:%M:%S %Z") [DEBU] $@" >&2; }
function info()  { echo "$(date +"%Y-%m-%d %H:%M:%S %Z") [INFO] $@" >&2; }

main $@
