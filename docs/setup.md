# Setup

`onion-grab` has been tested on Ubuntu/Debian based systems.  If you are running
a large measurement, you may run into issues that are **OS related**.

## Hints

  - We disabled and stopped `systemd-resolved`, which eventually causes some or
    all DNS requests to be blocked when running with many concurrent workers.
  - We used Google's `8.8.8.8` and `8.8.4.4`, which [supports 1500qps][] per IP.
  - You may need to tinker with `ulimit` and `sysctl`, e.g., if observing that
    there are too many open file descriptors or similar.  See for example the
    value of `ulimit -n` and `sysctl net.ipv4.ip_local_port_range`

[supports 1500qps]: https://developers.google.com/speed/public-dns/docs/isp

## Credit

Björn Töpel helped us debug some of these OS-related issues.
