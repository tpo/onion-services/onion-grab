# Notes

`onion-grab` uses use the default `net.Dial` function, which in turn uses
[goLookupIPCNAMEOrder][] for DNS lookups with the recursive name servers in
`/etc/resolve.conf`.  For example, with

    $ cat /etc/resolve.conf
    nameserver 8.8.8.8
    nameserver 8.8.4.4

[goLookupIPCNAMEOrder]: https://github.com/golang/go/blob/8edcdddb23c6d3f786b465c43b49e8d9a0015082/src/net/dnsclient_unix.go#L595-L804

the query will first be directed to `8.8.8.8`; then `8.8.4.4` if no valid answer
is available yet ([lines 663-778][]).  If you are running `onion-grab` with
[Mullvad VPN][], specify custom DNS: `mullvad dns set custom 8.8.8.8 8.8.4.4`.

[lines 663-778]: https://github.com/golang/go/blob/8edcdddb23c6d3f786b465c43b49e8d9a0015082/src/net/dnsclient_unix.go#L663-L778
[Mullvad VPN]: https://www.mullvad.net/

Further, default settings are used to follow at most 10 HTTP 3XX redirects.  A
[conservative][] value for the `MaxResponseHeaderBytes` option is 10MiB; the
`onion-grab` default is 16MiB and our measurements bumped this up to 64MiB.

[conservative]: https://go-review.googlesource.com/c/go/+/21329/2/src/net/http/transport.go
